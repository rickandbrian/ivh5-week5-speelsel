package edu.avans.ivh5.fysiotherapeut.businesslogic;

import edu.avans.ivh5.fysiotherapeut.domain.Book;

public interface bookManager {
	
	public Book findBook(int isbn);
	
	public Book removeBook(Book removeBook);
	
	public Book addBook(Book addBook);

}
