package edu.avans.ivh5.fysiotherapeut.datastorage;

import edu.avans.ivh5.fysiotherapeut.domain.Book;
import edu.avans.ivh5.fysiotherapeut.domain.Member;

import java.sql.ResultSet;
import java.sql.SQLException;

public class bookDAO {
	
	public bookDAO(){
		
	}
	
	
	public Book findBook(int isbn){
		Book book = null;
	       // First open a database connnection
        DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            // If a connection was successfully setup, execute the SELECT statement.
            ResultSet resultset = connection.executeSQLSelectStatement(
                    "SELECT * FROM book WHERE  = " + isbn + ";");

            if (resultset != null) {
                try {
                	
                    // The membershipnumber for a member is unique, so in case the
                    // resultset does contain data, we need its first entry.
                    if (resultset.next()) {
                        int isbnFromDb = resultset.getInt("isbn");
                        String titleFromDb = resultset.getString("title");
                        String authorFromDb = resultset.getString("author");
                        int editionFromDb = resultset.getInt("edition");

                        book = new Book(
                                isbnFromDb,
                                titleFromDb,
                                authorFromDb,
                                editionFromDb);

                    }
                } catch (SQLException e) {
                    System.out.println(e);
                    book = null;
                }
            }
            connection.closeConnection();
		}
        return book;
	}

}
