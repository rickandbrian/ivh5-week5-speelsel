/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.ivh5.fysiotherapeut.main;

import edu.avans.ivh5.fysiotherapeut.businesslogic.MemberAdminManagerImpl;
import edu.avans.ivh5.fysiotherapeut.businesslogic.bookManager;
import edu.avans.ivh5.fysiotherapeut.businesslogic.bookManagerupl;
import edu.avans.ivh5.fysiotherapeut.presentation.MemberAdminUI;

/**
 *
 * @author ppthgast
 */
public class Main {

    
    /**
     * private c'tor to prevent instantiation
     */
    private Main() {
       // deliberately empty
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        MemberAdminUI ui = new MemberAdminUI(new MemberAdminManagerImpl(), new bookManagerupl());
        ui.setVisible(true);
    }
}
